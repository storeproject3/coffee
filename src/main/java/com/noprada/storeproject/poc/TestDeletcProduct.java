/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.noprada.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 66945
 */
public class TestDeletcProduct {
    public static void main(String[] arg) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
           String aql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stml = conn.prepareStatement(aql);
            stml.setInt(1,5);
            int row = stml.executeUpdate();
            System.out.println("Affect now" +row);   
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }
}
