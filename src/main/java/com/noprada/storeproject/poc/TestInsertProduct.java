/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.noprada.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author 66945
 */
public class TestInsertProduct {
    public static void main(String[] arg) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
           String aql = "INSERT INTO product( name,price) VALUES (?, ?)";
            PreparedStatement stml = conn.prepareStatement(aql);
            Product product = new Product(-1,"Oh Leing", 20);
            stml.setString(1, product.getName());
            stml.setDouble(2,product.getPrice());
          
            int row = stml.executeUpdate();
            System.out.println("Affect now" +row);
            ResultSet result = stml.getGeneratedKeys();
           int id = -1;
           if(result.next()){
               id = result.getInt(1);
           }
            System.out.println("Affect now" +row + "id" + id);
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }
}
