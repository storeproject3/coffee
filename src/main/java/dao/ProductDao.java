/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.noprada.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author 66945
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String aql = "INSERT INTO product( name,price) VALUES (?, ?)";
            PreparedStatement stml = conn.prepareStatement(aql);
            stml.setString(1, object.getName());
            stml.setDouble(2, object.getPrice());

            int row = stml.executeUpdate();
            System.out.println("Affect now" + row);
            ResultSet result = stml.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,price FROM product";
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id, name, price);
                list.add(product);

            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,price FROM product Where id = " + id;
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            while (result.next()) {

                int pid = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(pid, name, price);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row =0;
        try {
           String aql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stml = conn.prepareStatement(aql);
            stml.setInt(1,id);
            row = stml.executeUpdate();
             
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row =0;
        try {
            String aql = "UPDATE product SET name = '?', price = '?' WHERE id = ?";
            PreparedStatement stml = conn.prepareStatement(aql);
            stml.setString(1, object.getName());
            stml.setDouble(2, object.getPrice());
            stml.setInt(3, object.getId());

            row = stml.executeUpdate();
            System.out.println("Affect now" + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] argd) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1, "Kafaeyen", 50.0));
        System.out.println("id: " + id);
        Product lastProduct = dao.get(id);
        System.out.println("lastProduct " + lastProduct);
        lastProduct.setPrice(100);
        int row =dao.update(lastProduct);
         Product updateProduct = dao.get(id);
        System.out.println("updateProduct " + updateProduct);
        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("deleteProduct " + deleteProduct);

    }
}
